// Encoding / Decoding functions
function stringToHex(plaintext) {
    return plaintext.split("").map(c => c.charCodeAt(0).toString(16).padStart(2, "0")).join("");
}

function hexToString(hex) {
    var str = '';
    for (var i = 0; i < hex.length; i += 2) {
        var v = parseInt(hex.substr(i, 2), 16);
        if (v) str += String.fromCharCode(v);
    }
    return str;
}

function stringToDecimal(plaintext) {
    return plaintext.split("").map(c => c.charCodeAt(0)).join(" ");
}

function decimalToString(decimal) {
    return decimal.split(" ").map(c => String.fromCharCode(c)).join("");
}

function urlEncodeToDoubleUrlEncode(urlEncode) {
    return urlEncode.replace(/%/g, "%25")
}

function doubleUrlEncodeToUrlEncode(urlEncode) {
	return urlEncode.replace(/%25/g, "%")
}

// Define text area elements
let areaPlaintext = document.getElementById('areaPlaintext');
let areaBase64 = document.getElementById('areaBase64');
let areaHexadecimal = document.getElementById('areaHexadecimal');
let areaDecimal = document.getElementById('areaDecimal');
let areaURLEncode = document.getElementById('areaURLEncode');
let areaDoubleURLEncode = document.getElementById('areaDoubleURLEncode');

// Events on input
areaBase64.oninput = function () {
    areaPlaintext.value = atob(areaBase64.value);
    areaHexadecimal.value = stringToHex(areaPlaintext.value);
    areaDecimal.value = stringToDecimal(areaPlaintext.value);
    areaURLEncode.value = encodeURIComponent(areaPlaintext.value);
    areaDoubleURLEncode.value = urlEncodeToDoubleUrlEncode(areaURLEncode.value);
}

areaPlaintext.oninput = function () {
    areaBase64.value = btoa(areaPlaintext.value);
    areaHexadecimal.value = stringToHex(areaPlaintext.value);
    areaDecimal.value = stringToDecimal(areaPlaintext.value);
    areaURLEncode.value = encodeURIComponent(areaPlaintext.value);
    areaDoubleURLEncode.value = urlEncodeToDoubleUrlEncode(areaURLEncode.value);
}

areaHexadecimal.oninput = function () {
    areaPlaintext.value = hexToString(areaHexadecimal.value);
    areaBase64.value = btoa(areaPlaintext.value);
    areaDecimal.value = stringToDecimal(areaPlaintext.value);
    areaURLEncode.value = encodeURIComponent(areaPlaintext.value);
    areaDoubleURLEncode.value = urlEncodeToDoubleUrlEncode(areaURLEncode.value);
}

areaDecimal.oninput = function () {
    areaPlaintext.value = decimalToString(areaDecimal.value);
    areaBase64.value = btoa(areaPlaintext.value);
    areaHexadecimal.value = stringToHex(areaPlaintext.value);
    areaURLEncode.value = encodeURIComponent(areaPlaintext.value);
    areaDoubleURLEncode.value = urlEncodeToDoubleUrlEncode(areaURLEncode.value);
}

areaURLEncode.oninput = function () {
    areaPlaintext.value = decodeURIComponent(areaURLEncode.value);
    areaBase64.value = btoa(areaPlaintext.value);
    areaHexadecimal.value = stringToHex(areaPlaintext.value);
    areaDecimal.value = stringToDecimal(areaPlaintext.value);
    areaDoubleURLEncode.value = urlEncodeToDoubleUrlEncode(areaURLEncode.value);
}

areaDoubleURLEncode.oninput = function () {
    areaURLEncode.value = doubleUrlEncodeToUrlEncode(areaDoubleURLEncode.value);
    areaPlaintext.value = decodeURIComponent(areaURLEncode.value);
    areaBase64.value = btoa(areaPlaintext.value);
    areaHexadecimal.value = stringToHex(areaPlaintext.value);
    areaDecimal.value = stringToDecimal(areaPlaintext.value);
}
