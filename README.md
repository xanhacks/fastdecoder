# Fast decoder

Web based encoder / decode made with native HTML / CSS and JS (~10KB).

## Demo

- [https://decoder.xanhacks.xyz](https://decoder.xanhacks.xyz)

## Integrations

- Plaintext
- Base64
- Hexadecimal
- Decimal (ASCII)
- URL encode
- Double URL encode

Todo :
- Handle error (try/catch)

## About

Inspired from [https://www.asciitohex.com/](https://www.asciitohex.com/).

## Friends

- [Fast HashCracker](https://gitlab.com/xanhacks/fasthashcracker)
